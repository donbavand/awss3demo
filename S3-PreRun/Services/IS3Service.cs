﻿using System.Threading.Tasks;
using Amazon.S3.Model;
using S3_PreRun.Models;

namespace S3_PreRun.Services
{
    public interface IS3Service
    {
        Task<S3Response> CreateBucketAsync(string bucketName);
        Task UploadFileAsync(string bucketName);
        Task GetObjectFromS3Async(string bucketName);
    }
}